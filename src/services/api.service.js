import axios from 'axios'

const apiKey = '9fb98527ed4b2f39026a8ba4ff0e1683'
const baseURL = 'http://api.themoviedb.org/3/movie/'

class ApiService {
    constructor() {
        this.resource = axios.create({
            baseURL
        })
        this.apiKey = `api_key=${apiKey}&language=en-US`
    }

    getPopular(currentPage) {
        return this.resource.get(`popular?${this.apiKey}&page=${currentPage}`)
    }

    getTopRated(currentPage) {
        return this.resource.get(`top_rated?${this.apiKey}&page=${currentPage}`)
    }

    getMovie(MovieId) {
        return this.resource.get(`${MovieId}?${this.apiKey}`)
    }
}

export default new ApiService()