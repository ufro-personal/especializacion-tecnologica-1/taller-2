import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        redirect: '/toprated'
    },
    {
        path: '/toprated',
        name: 'TopRated',
        component: () =>
            import ('../views/TopRated.vue')
    },
    {
        path: '/popular',
        name: 'Popular',
        component: () =>
            import ('../views/Popular.vue')
    },
    {
        path: '/movies/:id',
        name: 'Movie',
        component: () =>
            import ('../views/Movie.vue')
    }

]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router